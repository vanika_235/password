/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package password;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HP
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validatePassword method, of class PasswordValidator.
     */
    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validatePassword length regular");
        String password = "djfbvjsdhj";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
    @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword length regular");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
    @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword length regular");
        String password = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
    
    @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validatePassword length regular");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
    
    @Test
    public void testValidatePasswordSpecialCharRegular() {
        System.out.println("validatePassword the special character");
        String password = "djfbvj$sdhj";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
    
     @Test
    public void testValidatePasswordSpecialCharsException() {
        System.out.println("validatePassword the special character");
        String password = " ";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
      @Test
    public void testValidatePasswordSpecialCharsBoundaryIn() {
        System.out.println("validatePassword the special character");
        String password = "@#$%&+-hjh";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
    }
    @Test
    public void testValidatePasswordSpecialCharsBoundaryOut() {
        System.out.println("validatePassword the special character");
        String password = "@abjecjk";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
}
    @Test
    public void testValidatePasswordCheckDigitsRegular() {
        System.out.println("validatePassword the digits");
        String password = "1234gjhfg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
}
     @Test
    public void testValidatePasswordCheckDigitsException() {
        System.out.println("validatePassword the digits");
        String password = " ";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
    }
     @Test
    public void testValidatePasswordCheckDigitsBoundaryIn() {
        System.out.println("validatePassword the digits");
        String password = "1ghjs4fg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
        
}
      @Test
    public void testValidatePasswordCheckDigitsBoundaryOut() {
        System.out.println("validatePassword the digits");
        String password = "34";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
    }
      @Test
    public void testValidatePasswordLowerCaseRegular() {
        System.out.println("validatePassword the lower case");
        String password = "1ghjs4fg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
    }
      @Test
    public void testValidatePasswordLowerCaseException() {
        System.out.println("validatePassword thelower case");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
    }
      @Test
    public void testValidatePasswordLowerCaseBoundaryIn() {
        System.out.println("validatePassword the lower case");
        String password = "1ghjs4fg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
    }
     @Test
    public void testValidatePasswordLowerCaseBoundaryOut() {
        System.out.println("validatePassword the lower case");
        String password = "1ghjf&hjs4fg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("the length of the password does not meet the requirement" ,expResult, result);
    }
}
